@extends('frontend.template.root')

@section('content')
    @include('frontend.template.header')
    @include('frontend.website.pricing')
    @include('frontend.website.blog')
@endsection