@extends('frontend.template.root')

@section('content')
<header class="blog-hero-area" style="background: url({{ $product->thumb }}) no-repeat;">
    <div class="overlay-light"></div>
    <div class="navbar-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <a href="{{ route('website.index') }}" target="_blade" class="navbar-brand">
                            <img src="{{ URL::asset('asset/img/logoht.png') }}" alt="Logo">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto w-100 justify-content-end">
                                <li class="nav-item">
                                    <a class="nav-link page-scroll active" href="{{ route('website.index') }}#home">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link page-scroll" href="{{ route('website.index') }}#pricing">Pricing</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link page-scroll" href="{{ route('website.index') }}#blog">Blog</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div id="home">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-lg-10">
                    <div class="contents text-center">
                        <h2 class="wow fadeIn" data-wow-duration="1000ms" data-wow-delay="0.3s">{{ $product->title }}</h2>
                        <div class="post-meta">
                            <ul>
                                {{-- <li><i class="lni lni-calendar"></i> <a href="#">{{ date('m/d/Y', strtotime($product->updated_at)) }}</a>
                                </li> --}}
                                <li><i class="lni lni-user"></i> <a href="#">{{ $product->price }}</a>
                                </li>
                                <li><i class="lni lni-bubble"></i> <a href="#">{{ $product->view }} Lượt xem</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div id="blog-single">
    <div class="container">
        {!! $product->content !!}    
    </div>
</div>
@endsection